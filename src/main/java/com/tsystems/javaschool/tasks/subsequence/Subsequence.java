package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")

    public boolean find(List x, List y) {
        boolean result = false;
        int foundWords = 0, start = 0;

        if (x != null && y != null) {
search:    for (Object wordToSearch : x) {
            wordToSearch.toString();
                for (int j = start; j < y.size(); j++) {
                    if (wordToSearch.equals(y.get(j))) {
                        start = y.indexOf(y.get(j)) + 1;
                        foundWords++;
                        continue search;
                    }
                }
                break search;
            }

            if (foundWords == x.size()) {
                result = true;
            }

        } else {
            throw new IllegalArgumentException("Arguments cannot be null");
        }


        return result;
    }

}
