package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null) || inputNumbers.contains("")) {
            throw new CannotBuildPyramidException();
        }

        // Check for duplicates
        Set<Integer> set = new HashSet<Integer>(inputNumbers);
        if(set.size() < inputNumbers.size()){
            throw new CannotBuildPyramidException();
        }

        int rows = 1;
        int columns = 1;

        // Figure out dimensions of output array
        while (columns <= inputNumbers.size() - columns) {
            rows++;
            columns += 2;
        }

        Collections.sort(inputNumbers);

        int[][] result = new int[rows][columns];

        boolean print = true;
        ArrayList<Integer> output = new ArrayList<>();

        Iterator<Integer> iter = inputNumbers.iterator();

        for (int i = 1; i <= rows; i++) {

            // Print 0 in decreasing order
            for (int j = rows; j > i; j--) {
                output.add(0);
            }
            // Print nums in increasing order
            for (int k = 1; k <= (i * 2) - 1; k++) {
                if(print) {
                    if (iter.hasNext()) {
                        output.add(iter.next());
                    }
                    print = false;
                } else {
                    output.add(0);
                    print = true;
                }

            }

            // Print 0 in decreasing order
            for (int j = rows; j > i; j--) {
                output.add(0);
            }

            // End of line
            print = true;
        }

        // Bottom right num should not be a 0
        if (output.get(output.size() -1) == 0) {
            throw new CannotBuildPyramidException();
        }

        Iterator<Integer> iter2 = output.iterator();

        // Fill output 2dim array from list
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (iter2.hasNext()) {
                    result[i][j] = iter2.next();
                }
            }
        }
        return result;
    }
}