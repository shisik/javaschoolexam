package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */


    public String evaluate(String statement) {

        if (statement == null || statement.isEmpty()) {
            return null;
        }

        int numOfBrackets = 0;
        String input = "";

        // Normalize string and check if it's correct
        for (int i = 0; i < statement.length(); i++) {

            char temp = statement.charAt(i);
            if (temp == '(') {
                numOfBrackets++;
                input = input + temp + " ";
            }
            if (temp == ')') {
                numOfBrackets++;
                input = input + " " + temp;
            }
            if (temp == '+' || temp == '-' || temp == '*' || temp == '/') {
                if (statement.charAt((i + 1)) == '+' || statement.charAt((i + 1)) == '-' || statement.charAt((i + 1)) == '*' || statement.charAt((i + 1)) == '/') {
                    return null;
                }
                input = input + " " + temp + " ";
            }
            if (Character.isDigit(temp)) {
                input = input + temp;
            }
            if (temp == '.') {
                if (statement.charAt((i + 1)) == '.') {
                    return null;
                }
                input = input + ".";
            }
            if (temp == ',') {
                return null;
            }
        }


        if (numOfBrackets % 2 != 0) {
            return  null;
        }

        List<String> strList = new ArrayList<>();
        for (String listElement : input.trim().split(" ")) {
            strList.add(listElement);
            strList.add(" ");
        }
        strList.remove(strList.size() - 1);

        if (strList.indexOf("(") != -1) {

            if (strList.indexOf("(") > strList.indexOf(")")) {
                return null;
            }
            for (int i = strList.indexOf("(") + 1; i < strList.size() - 1; i++) {

                String recursion = "";
                if (strList.get(i).equals("(")) {
                    for (int j = i; j < strList.lastIndexOf(")"); j++) {
                        recursion += strList.get(j);
                    }
                    String test = input.substring(input.indexOf("("), input.lastIndexOf(")") + 1);

                    String testRecursion = String.valueOf(evaluate(recursion));
                    input = input.replace(test, testRecursion);
                    strList.removeAll(strList);
                    for (String newElement : input.trim().split(" ")) {
                        strList.add(newElement);
                        strList.add(" ");
                    }
                }

                String recursion2 = "";
                if (strList.get(i).equals(")")) {

                    for (int j = strList.indexOf("(") + 1; j < strList.indexOf(")"); j++) {
                        recursion2 += strList.get(j);
                    }
                    String test2 = input.substring(input.indexOf("("), input.lastIndexOf(")") + 1);
                    String testRecursion2 = String.valueOf(evaluate(recursion2));
                    input = input.replace(test2, testRecursion2);
                    for (String newElement : input.trim().split(" ")) {
                        strList.add(newElement);
                        strList.add(" ");
                    }
                }
            }
        }

        List<String> stringList2 = new ArrayList<>();
        for (String element : input.trim().split(" ")) {
            stringList2.add(element);
        }


        while (stringList2.size() != 0) {

            Double result = 0d;

            if (stringList2.indexOf("/") != -1) {
                int index = stringList2.indexOf("/");
                try {
                    Double.parseDouble(stringList2.get(index - 1));
                    Double.parseDouble(stringList2.get(index + 1));
                } catch(NumberFormatException e){
                    return null;
                }

                result = Double.valueOf(stringList2.get(index - 1)) / Double.valueOf(stringList2.get(index + 1));
                stringList2.add(index - 1, String.valueOf(result));
                stringList2.remove(index + 2);
                stringList2.remove(index + 1);
                stringList2.remove(index);
            }
            else if (stringList2.indexOf("*") != -1) {
                int index = stringList2.indexOf("*");
                result = Double.valueOf(stringList2.get(index - 1)) * Double.valueOf(stringList2.get(index + 1));
                stringList2.add(index - 1, String.valueOf(result));
                stringList2.remove(index + 2);
                stringList2.remove(index + 1);
                stringList2.remove(index);
            }
            else if (stringList2.indexOf("-") != -1) {
                int index = stringList2.indexOf("-");
                int lastIndex = stringList2.lastIndexOf("-");
                if (index == 0) {
                    result = 0.0 - Double.parseDouble(stringList2.get(index + 1));
                    stringList2.add(0, String.valueOf(result));
                    stringList2.remove(2);
                    stringList2.remove(1);
                }
                else if ((lastIndex-2>0) && (stringList2.get(lastIndex-2).equals("-"))){
                    result = Double.parseDouble(stringList2.get(lastIndex + 1)) + Double.parseDouble(stringList2.get(lastIndex - 1));
                    stringList2.add(lastIndex - 1, String.valueOf(result));
                    stringList2.remove(lastIndex + 2);
                    stringList2.remove(lastIndex + 1);
                    stringList2.remove(lastIndex);
                }
                else {
                    result = Double.parseDouble(stringList2.get(index - 1)) - Double.parseDouble(stringList2.get(index + 1));
                    stringList2.add(index - 1, String.valueOf(result));
                    stringList2.remove(index + 2);
                    stringList2.remove(index + 1);
                    stringList2.remove(index);
                }
            }
            else if (stringList2.indexOf("+") != -1) {
                int index = stringList2.indexOf("+");
                result = Double.parseDouble(stringList2.get(index - 1)) + Double.parseDouble(stringList2.get(index + 1));
                stringList2.add(index - 1, String.valueOf(result));
                stringList2.remove(index + 2);
                stringList2.remove(index + 1);
                stringList2.remove(index);
            }

            if ((stringList2.indexOf("*") == -1) && (stringList2.indexOf("/") == -1) && (stringList2.indexOf("+") == -1) && (stringList2.indexOf("-") == -1)) {
                if (result % 1 == 0) {
                    return String.valueOf(result.intValue());
                }
                return String.valueOf(result);
            }
        }
        if (Double.valueOf(stringList2.get(0)) % 1 == 0) {
            return String.valueOf(Double.valueOf(stringList2.get(0)).intValue());
        }
        return stringList2.get(0);
    }
}
